import { Component } from '@angular/core';
import { SceneService } from "./components/scene/scene.service";

@Component({
  selector: 'agk-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(private _sceneService: SceneService) { }

    resize(): void {
      this._sceneService.updateViewportSize();
    }
}
