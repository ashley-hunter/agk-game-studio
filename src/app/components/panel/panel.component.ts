import { Component, Input } from '@angular/core';

@Component({
    selector: 'agk-panel',
    templateUrl: './panel.component.html',
    styleUrls: ['./panel.component.scss']
})
export class PanelComponent {

    @Input() header: string;
}