import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { ToolbarComponent } from './toolbar/toolbar.component';
import { PanelComponent } from "./panel/panel.component";
import { SplitterComponent } from "./splitter/splitter.component";
import { SplitterDirective } from "./splitter/splitter.directive";
import { PropertiesComponent } from "./properties/properties.component";
import { SceneComponent } from "./scene/scene.component";
import { SceneService } from "./scene/scene.service";

const DECLARATIONS = [
    ToolbarComponent,
    PanelComponent,
    SplitterComponent,
    SplitterDirective,
    PropertiesComponent,
    SceneComponent
];

@NgModule({
    imports: [
        CommonModule
    ],
    exports: DECLARATIONS,
    declarations: DECLARATIONS,
    providers: [
        SceneService
    ]
})
export class ComponentsModule { }
