import { Component, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy, ContentChildren, QueryList, TemplateRef, Output, EventEmitter } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/switchMap';
import { SplitterDirective } from "./splitter.directive";

@Component({
    selector: 'agk-splitter',
    templateUrl: './splitter.component.html',
    styleUrls: ['./splitter.component.scss'],
    host: {
        '[class]': 'orientation'
    }
})
export class SplitterComponent implements AfterViewInit, OnDestroy {

    @Input() orientation: Orientation = 'horizontal';
    @Input() ratio: number = 0.5;
    @Input() min: number = 200;
    @Output() resize: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('splitter') splitter: ElementRef;
    @ContentChildren(SplitterDirective, { read: TemplateRef }) panels: QueryList<SplitterDirective>;

    private _subscription: Subscription;

    constructor(private _elementRef: ElementRef) { }

    get minWidth(): number {
        return this.orientation === 'horizontal' ? this.min : null;
    }

    get minHeight(): number {
        return this.orientation === 'vertical' ? this.min : null;
    }

    ngAfterViewInit(): void {
        let mousedown$ = Observable.fromEvent(this.splitter.nativeElement, 'mousedown');
        let mousemove$ = Observable.fromEvent(document, 'mousemove');
        let mouseup$ = Observable.fromEvent(document, 'mouseup');

        this._subscription = mousedown$.switchMap(event => mousemove$.takeUntil(mouseup$))
            .subscribe(this.drag.bind(this));

        // emit resize event
        this.resize.emit();
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }

    drag(event: MouseEvent): void {

        let position: number = this.orientation === 'horizontal' ? event.pageX - this._elementRef.nativeElement.getBoundingClientRect().left : event.pageY - this._elementRef.nativeElement.getBoundingClientRect().top;
        let size: number = this.orientation === 'horizontal' ? this._elementRef.nativeElement.offsetWidth : this._elementRef.nativeElement.offsetHeight;

        // calculate flex value
        this.ratio = Math.min(position / size, 1);

        // emit resize event
        this.resize.emit();
    }
}

export type Orientation = 'horizontal' | 'vertical';