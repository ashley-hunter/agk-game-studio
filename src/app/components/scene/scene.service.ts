import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";

@Injectable()
export class SceneService {

    resize: Subject<void> = new Subject<void>();

    updateViewportSize(): void {
        this.resize.next();
    }
}