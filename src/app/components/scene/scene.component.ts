import { Component, ElementRef, HostListener, AfterViewInit, NgZone } from '@angular/core';
import { Scene, PerspectiveCamera, WebGLRenderer } from "three";
import { SceneService } from "./scene.service";
import { Viewport } from "./components/viewport";
import { InputManager, Key, MouseButton } from "./components/input-manager";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'agk-scene',
    templateUrl: './scene.component.html',
    styleUrls: ['./scene.component.scss']
})
export class SceneComponent implements AfterViewInit {

    private _viewport: Viewport;
    private _inputManager: InputManager;

    private _subscription$: Subscription;

    constructor(private _elementRef: ElementRef, private _sceneService: SceneService, private _ngZone: NgZone) {

        let bounds: ClientRect = this._elementRef.nativeElement.getBoundingClientRect();

        this._viewport = new Viewport(this._elementRef.nativeElement, bounds.width, bounds.height);
        this._inputManager = this._viewport.getInputManager();

        // add the canvas to the element
        _elementRef.nativeElement.appendChild(this._viewport.getElement());

        // subscribe to size changes
        _sceneService.resize.subscribe(this.updateViewportSize.bind(this));

        // subscribe to mouse events
        let mousedown = Observable.fromEvent<MouseEvent>(this._elementRef.nativeElement, 'mousedown');
        let mousemove = Observable.fromEvent<MouseEvent>(document, 'mousemove');
        let mouseup = Observable.fromEvent<MouseEvent>(document, 'mouseup');

        // subscribe to the right mouse drag
        mousedown.filter(event => event.button === MouseButton.Right)
            .switchMap(event => mousemove.takeUntil(mouseup))
            .subscribe(event => this._inputManager.rightMouseDrag.next(event));

    }

    ngAfterViewInit(): void {

        // update the scene size
        this.updateViewportSize();

        // start rendering loop
        this._ngZone.runOutsideAngular(() => this._viewport.render());
    }

    @HostListener('window:resize')
    updateViewportSize(): void {

        let bounds: ClientRect = this._elementRef.nativeElement.getBoundingClientRect();
        this._viewport.resizeViewport(bounds.width, bounds.height);
    }

    /**
     * Detect input
     */

    @HostListener('document:keydown', ['$event.keyCode'])
    onKeyDown(keyCode: Key) {
        this._inputManager.setKeyState(keyCode, true);
    }

    @HostListener('document:keyup', ['$event.keyCode'])
    onKeyUp(keyCode: Key) {
        this._inputManager.setKeyState(keyCode, false);
    }

    @HostListener('mousedown', ['$event', '$event.button'])
    onMouseDown(event: MouseEvent, button: MouseButton) {

        if (button === MouseButton.Right) {
            this._viewport.getElement().requestPointerLock();
        }
    }

    @HostListener('mouseup', ['$event'])
    onMouseUp(event: MouseEvent) {
        document.exitPointerLock();
    }


}