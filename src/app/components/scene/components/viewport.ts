import { Scene, GridHelper, PerspectiveCamera, Renderer, WebGLRenderer, Color } from "three";
import { InputManager, Key } from "./input-manager";
import { SceneManager } from "./scene-manager";
import { FirstPersonControls } from './controls/first-person-camera';

const GRID_SIZE = 10000;
const GRID_DIVISIONS = 1000;

export class Viewport {

    private _scene: Scene;
    private _camera: PerspectiveCamera;
    private _renderer: WebGLRenderer;
    private _inputManager = new InputManager();
    private _sceneManager: SceneManager;
    private _controls: FirstPersonControls;

    private _grid: GridHelper;
    private _width: number;
    private _height: number;

    private _fps: number = 0;
    private _lastFrame: number = 0;

    constructor(private _domElement: HTMLElement, width: number, height: number) {

        this._scene = new Scene();
        this._camera = new PerspectiveCamera( 75, width / height, 0.1, 10000 );
        this._renderer = new WebGLRenderer({ antialias: true });
        this._sceneManager = new SceneManager(this._scene);
        this._controls = new FirstPersonControls(this._camera, _domElement);

        // setup the initial scene
        this._scene.background = new Color('#474747');
        this._camera.position.y = 20;
        
        // create the grid
        this._grid = new GridHelper( GRID_SIZE, GRID_DIVISIONS ); 
        
        // add the grid to the scene
        this._scene.add(this._grid);

        this._sceneManager.makeCube();
    }

    render(): void {
        // begin request for next frame
        requestAnimationFrame(this.render.bind(this));

        // perform any camera updates
        this._controls.update(2);

        // render the current frame
        this._renderer.render(this._scene, this._camera);

        // update the fps counter
        this.updateFps();
    }

    getElement(): HTMLCanvasElement {
        return this._renderer.domElement;
    }

    resizeViewport(width: number, height: number): void {

        // store the size values
        this._width = width;
        this._height = height;

        // update the camera
        this._camera.aspect = this._width / this._height;
        this._camera.updateProjectionMatrix();
    
        // set the renderer size
        this._renderer.setSize( this._width, this._height );
    }

    private updateFps(): void {

        let now = performance.now();
        let delta = now - this._lastFrame;

        this._fps = 1000 / delta;
        this._lastFrame = now;
    }

    getFps(): number {
        return this._fps;
    }

    getInputManager(): InputManager {
        return this._inputManager;
    }
}