/**
 * FirstPersonControls class
 *
 * @author mrdoob / http://mrdoob.com/
 * @author alteredq / http://alteredqualia.com/
 * @author paulirish / http://paulirish.com/
 */
import { Vector3, Math as Math3, Object3D } from "three";

export class FirstPersonControls {

    enabled: boolean = false;

    private _movementSpeed: number = 1.0;
    private _lookSpeed: number = 0.005;

    private _target: Vector3 = new Vector3(0, 0, 0);

    private _mouseX: number = 0;
    private _mouseY: number = 0;

    private _lat: number = 0;
    private _lon: number = 0;
    private _phi: number = 0;
    private _theta: number = 0;

    private _moveForward: boolean = false;
    private _moveBackward: boolean = false;
    private _moveLeft: boolean = false;
    private _moveRight: boolean = false;
    private _moveUp: boolean = false;
    private _moveDown: boolean = false;
    private _mouseDragOn: boolean = false;

    constructor(private _object: Object3D, private _domElement: HTMLElement) {
        this._domElement.setAttribute('tabindex', '-1');
        this.bindEvents();
    }

    /**
     * BindEvents function
     */
    bindEvents() {

        this._domElement.addEventListener('contextmenu', this.contextMenu.bind(this), false);
        this._domElement.addEventListener('mousemove', this.onMouseMove.bind(this), false);
        this._domElement.addEventListener('mousedown', this.onMouseDown.bind(this), false);
        this._domElement.addEventListener('mouseup', this.onMouseUp.bind(this), false);

        window.addEventListener('keydown', this.onKeyDown.bind(this), false);
        window.addEventListener('keyup', this.onKeyUp.bind(this), false);
    }

    onMouseDown(event: MouseEvent) {

        this._domElement.focus();

        event.preventDefault();
        event.stopPropagation();

        switch (event.button) {
            case 0:
                // this._moveForward = true;
                break;

            case 2:
                this.enabled = true;
                break;
        }

        this._mouseDragOn = true;
    }

    /**
     * OnMouseUp function
     * @param  {object} event Event
     */
    onMouseUp(event: MouseEvent) {

        event.preventDefault();
        event.stopPropagation();

        this.enabled = false;

        switch (event.button) {

            case 0:
                this._moveForward = false;
                break;

            case 2:
                this._moveBackward = false;
                break;
        }

        this._mouseDragOn = false;
    }

    /**
     * OnMouseMove function
     * @param  {object} event Event
     */
    onMouseMove(event: MouseEvent) {
        this._mouseX = event.movementX * 100;
        this._mouseY = event.movementY * 100;
    }

    /**
     * OnKeyDown function
     * @param  {object} event Event
     */
    onKeyDown(event: KeyboardEvent) {

        switch (event.keyCode) {

            case 38: /*up*/
            case 87: /*W*/
                this._moveForward = true;
                break;

            case 37: /*left*/
            case 65: /*A*/
                this._moveLeft = true;
                break;

            case 40: /*down*/
            case 83: /*S*/
                this._moveBackward = true;
                break;

            case 39: /*right*/
            case 68: /*D*/
                this._moveRight = true;
                break;

            case 82: /*R*/
                this._moveUp = true;
                break;

            case 70: /*F*/
                this._moveDown = true;
                break;
        }
    }

    /**
     * OnKeyUp function
     * @param  {object} event Event
     */
    onKeyUp(event: KeyboardEvent) {

        switch (event.keyCode) {

            case 38: /*up*/
            case 87: /*W*/
                this._moveForward = false;
                break;

            case 37: /*left*/
            case 65: /*A*/
                this._moveLeft = false;
                break;

            case 40: /*down*/
            case 83: /*S*/
                this._moveBackward = false;
                break;

            case 39: /*right*/
            case 68: /*D*/
                this._moveRight = false;
                break;

            case 82: /*R*/
                this._moveUp = false;
                break;

            case 70: /*F*/
                this._moveDown = false;
                break;
        }
    }

    /**
     * Update function
     * @param  {object} delta Delta
     */
    update(delta: number) {


        let actualMoveSpeed = delta * this._movementSpeed;

        if (this._moveForward) this._object.translateZ(-actualMoveSpeed);
        if (this._moveBackward) this._object.translateZ(actualMoveSpeed);

        if (this._moveLeft) this._object.translateX(- actualMoveSpeed);
        if (this._moveRight) this._object.translateX(actualMoveSpeed);

        if (this._moveUp) this._object.translateY(actualMoveSpeed);
        if (this._moveDown) this._object.translateY(- actualMoveSpeed);

        if (this.enabled === false) {
            return;
        }

        let actualLookSpeed = delta * this._lookSpeed;

        let verticalLookRatio = 1;

        this._lon += this._mouseX * actualLookSpeed;
        this._lat -= this._mouseY * actualLookSpeed * verticalLookRatio;

        this._lat = Math.max(- 85, Math.min(85, this._lat));
        this._phi = Math3.degToRad(90 - this._lat);

        this._theta = Math3.degToRad(this._lon);

        let targetPosition = this._target,
            position = this._object.position;

        targetPosition.x = position.x + 100 * Math.sin(this._phi) * Math.cos(this._theta);
        targetPosition.y = position.y + 100 * Math.cos(this._phi);
        targetPosition.z = position.z + 100 * Math.sin(this._phi) * Math.sin(this._theta);

        this._object.lookAt(targetPosition);

        // reset the mouse movement values
        this._mouseX = 0;
        this._mouseY = 0;
    }

    /**
     * ContextMenu function
     * @param  {object} event Event
     */
    contextMenu(event: MouseEvent) {
        event.preventDefault();
    }

    /**
     * Dispose function
     */
    dispose() {

        this._domElement.removeEventListener('contextmenu', this.contextMenu.bind(this), false);
        this._domElement.removeEventListener('mousedown', this.onMouseDown.bind(this), false);
        this._domElement.removeEventListener('mousemove', this.onMouseMove.bind(this), false);
        this._domElement.removeEventListener('mouseup', this.onMouseUp.bind(this), false);

        window.removeEventListener('keydown', this.onKeyDown.bind(this), false);
        window.removeEventListener('keyup', this.onKeyUp.bind(this), false);
    }
}