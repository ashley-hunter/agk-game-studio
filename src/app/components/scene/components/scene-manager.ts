import { Scene, SphereGeometry, Mesh, BoxHelper, MeshBasicMaterial, Color } from "three";

export class SceneManager {

    private _entities: any[];

    constructor(private _scene: Scene) { }

    makeCube(): void {
        var sphere = new SphereGeometry(10);
        var object = new Mesh( sphere, new MeshBasicMaterial({
            color: '#ff0000'
        }) );
        var box = new BoxHelper( object, new Color('#ffff00'));

        this._scene.add(object);
        this._scene.add(box);
    }

}