import { Subject } from "rxjs/Subject";

export class InputManager {

    private _state: { [key: string]: boolean } = {};

    rightMouseDrag: Subject<MouseEvent> = new Subject<MouseEvent>();

    setKeyState(key: Key, state: boolean): void {
        this._state[key] = state;
    }

    getKeyState(key: Key): boolean {
        return this._state[key] ? this._state[key]: false;
    }

}

export enum Key {
    Left = 37,
    Up = 38,
    Right = 39,
    Down = 40
}

export enum MouseButton {
    Left = 0,
    Right = 2
}